#include <string>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <fcntl.h>
#include <string.h>
#include <cstring>
#include <stdexcept>
#include <malloc.h>

#include "db_access.h"

void dummy(const char *)
{
}

db_entity::db_entity(const char * my_data, size_t my_length, bool dealloc):
data_length(my_length),
data_ptr(my_data),
dealloc(dealloc)
{
	//data_ptr = new char[my_length];
	//std::memcpy(data_ptr, my_data, my_length);
}

db_entity::db_entity(db_entity && orig):
data_length(orig.data_length),
data_ptr(orig.data_ptr),
dealloc(orig.dealloc)
{
	orig.dealloc = false;
}

db_entity::~db_entity()
{
	if(dealloc)
	{
	free((void *)data_ptr);
	}
}

const char *
db_entity::data()
{
	return data_ptr;
}

size_t
db_entity::length()
{
	return data_length;
}

bool
db_entity::is_valid()
{
	return data_ptr != NULL;
}

db_access::db_access(std::string filename)
{
	db_ptr = gdbm_open((char *) filename.c_str(), 1024, GDBM_WRCREAT, 0640, &dummy);
	if(!db_ptr)
	{
		throw std::runtime_error("Could not open database " + filename);
	}
}

db_access::~db_access()
{
	gdbm_close(db_ptr);
}

db_entity
db_access::fetch(db_entity &ent)
{
	datum key, ret;
	key.dptr = (char *) ent.data();
	key.dsize = ent.length();
	ret = gdbm_fetch(db_ptr, key);
	return db_entity(ret.dptr, ret.dsize, true);
}

void
db_access::store(db_entity &key_ent, db_entity &value_ent, bool replace)
{
	datum key, value;
	key.dptr = (char *) key_ent.data();
	key.dsize = key_ent.length();
	value.dptr = (char *) value_ent.data();
	value.dsize = value_ent.length();
	int ret = 0;
	if(replace)
	{
		ret = gdbm_store(db_ptr, key, value, GDBM_REPLACE);
	}
	else
	{
		ret = gdbm_store(db_ptr, key, value, GDBM_INSERT);
	}
	if(ret != 0)
	{
		throw std::runtime_error("gdbm_store failed!");
	}
}

