#include <iostream>
#include <fstream>
//#include <sys/types.h>
#include <osmium/io/any_input.hpp>
#include <osmium/handler.hpp>
#include <osmium/osm/node.hpp>
#include <osmium/osm/node.hpp>
#include <osmium/osm/way.hpp>
#include <osmium/visitor.hpp>
#include <list>
#include <cstdint>
#include <utility>

#include "db_access.h"

db_access nodes_db("nodes.db");
db_access noderefs("noderefs.db");
db_access areas("areas.db");
db_access relations("relations.db");


void
record_tag(
  osmium::object_id_type id,
  std::string & key,
  std::string & value)
{
  db_access db(key + ".tags.db");
  db_entity tmp(value.c_str(), value.length());
  db_entity tmp2(db.fetch(tmp));
  char * new_buf = nullptr;
  if(tmp2.is_valid())
  {
    new_buf = new char[tmp2.length() + sizeof(id)];
    std::memcpy(new_buf, tmp2.data(), tmp2.length());
    *((osmium::object_id_type *)(new_buf + tmp2.length())) = id;
    db_entity tmp3(new_buf, tmp2.length() + sizeof(id));
    db.store(tmp, tmp3, true);
  }
  else
  {
    //std::cout << " " << std::to_string(id) << ": " << key << ": " << value << std::endl;
    new_buf = new char[sizeof(id)];
    *((osmium::object_id_type *)new_buf) = id;
    db_entity tmp3(new_buf, sizeof(id));
    db.store(tmp, tmp3, false);
  }
  delete[] new_buf;
}

void
record_taglist_to_db(
  osmium::object_id_type id,
  const osmium::TagList & list)
{
  for(auto &t : list)
    {
      std::string key(t.key());
      std::string value(t.value());
      record_tag(id, key, value);
    }
}

void
record_nodereflist_to_db(osmium::object_id_type id, const osmium::NodeRefList & list)
{
  osmium::object_id_type * buf = new osmium::object_id_type[list.size()];
  int i = 0;
  for(auto & r : list)
  {
    buf[i++] = r.ref();
  }
  db_entity value((char *) buf, list.size()*sizeof(osmium::object_id_type));
  db_entity key((char *) &id, sizeof(id));
  noderefs.store(key, value, false);
  delete[] buf;
}

void
record_nodelocation_to_db(const osmium::Node & node)
{
  osmium::object_id_type id = node.id();
  std::int32_t location[2];
  location[0] = node.location().x();
  location[1] = node.location().y();
  db_entity key((char *) &id, sizeof(id));
  db_entity value((char *)location, sizeof(std::int32_t)*2);
  nodes_db.store(key, value, false);
}


class MyHandler : public osmium::handler::Handler
{
  public:

  void node(const osmium::Node & node)
  {
    const osmium::TagList &list = node.tags();
    osmium::object_id_type id = node.id();
    record_taglist_to_db(id, list);
    record_nodelocation_to_db(node);
  }
  
  void way(const osmium::Way & way)
  {
    const osmium::TagList &list = way.tags();
    osmium::object_id_type id = way.id();
    record_taglist_to_db(id, list);
    const osmium::NodeRefList &nodes = way.nodes();
    record_nodereflist_to_db(id, nodes);
  }
  
  void area(const osmium::Area & area)
  {
    const osmium::TagList &list = area.tags();
    osmium::object_id_type id = area.id();
    record_taglist_to_db(id, list);
    int outer_iter = 0;
    for(auto & outer : area.outer_rings())
    {
      const std::string key_name = std::to_string(area.id()) + "." + std::to_string(outer_iter);
      const std::size_t element_count = outer.size();
      osmium::object_id_type * buf = new osmium::object_id_type[element_count];
      std::size_t i = 0;
      for(auto & r : outer)
      {
        buf[i++] = r.ref();
      }
      db_entity key(key_name.c_str(), key_name.length());
      db_entity value((char *) buf, element_count * sizeof(osmium::object_id_type));
      areas.store(key, value, false);
      delete[] buf;
      int inner_iter = 0;
      for(auto &inner : area.inner_rings(outer))
      {
        std::string key_name = std::to_string(area.id()) + "." + std::to_string(outer_iter) + std::to_string(inner_iter);
        const std::size_t element_count = inner.size();
        osmium::object_id_type * buf = new osmium::object_id_type[element_count];
        std::size_t i = 0;
        for(auto & r : outer)
        {
          buf[i++] = r.ref();
        }
        db_entity key(key_name.c_str(), key_name.length());
        db_entity value((char *)buf, element_count*sizeof(osmium::object_id_type));
        areas.store(key, value, false);
        delete[] buf;
        inner_iter++;
      }
      outer_iter++;
    }
  }
  
  void relation(const osmium::Relation & rel)
  {
    const osmium::TagList &list = rel.tags();
    osmium::object_id_type id = rel.id();
    record_taglist_to_db(id, list);
    osmium::object_id_type * buf = new osmium::object_id_type[rel.members().size()];
    std::size_t i = 0;
    for(auto & m : rel.members())
    {
      buf[i++] = m.ref();
    }
    db_entity key((char *) &id, sizeof(id));
    db_entity value((char *) buf, rel.members().size() * sizeof(osmium::object_id_type));
    relations.store(key, value, false);
    delete[] buf;
  }
};

int main(int argc, char *argv[])
{
  auto otypes = osmium::osm_entity_bits::node | osmium::osm_entity_bits::way;
  osmium::io::File input_file{"germany-latest.osm.pbf"};
  osmium::io::Reader reader(input_file);
  MyHandler handler;

  osmium::apply(reader, handler);
  reader.close();


  return 0;
}
