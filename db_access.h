#ifndef _db_access_h
#define _db_access_h

#include <string>
#include <gdbm.h>

class db_entity
{
	public:
	db_entity(const char *, size_t, bool dealloc = false);
	db_entity(db_entity &&);
	~db_entity();
	
	const char * data();
	size_t length();
	
	bool is_valid();
	
	private:
	const char * data_ptr;
	size_t data_length;
	bool dealloc;
};

class db_access
{
	public:
	db_access(std::string filename);
	~db_access();
	
	db_entity fetch(db_entity&);
	void store(db_entity&, db_entity&, bool);
	
	private:
	GDBM_FILE db_ptr;
};
#endif
